
1. Build setup
	virtualenv -p python3 venv
2. pip3 install coverage flake8
3. flake8 sample_project/*py
4. flake8 tests/*py
5. python setup.py install
6. coverage3 run setup.py test
7. coverage report -m -i --omit=venv/*,.eggs/*,/tmp/*
