from setuptools import setup, find_packages

setup(
    name = 'sample_project',
    version = '0.0.1',
    python_requires='>3.5.2',
   
    description = 'sample_project',
    license = 'GPLv2',

    author = 'Hiren Chandratre',
    author_email = 'hiren.chandratre@gmail.com',

    packages = find_packages(
        exclude=['tests']
    ),

    test_suite = 'tests',

    install_requires = [
        'boto3'
    ],

    tests_require = [
        'moto'
    ],

    entry_point = {
        'console_scripts': [
            's3 = sample_project.s3.main',
        ]
    },
)
